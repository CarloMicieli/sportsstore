using System;
using System.Net;
using System.Net.Http;
using Xunit;
using Microsoft.AspNetCore.Mvc.Testing;
using Microsoft.AspNetCore.TestHost;

namespace ServerApp.Tests
{
    public class UnitTest1 : IClassFixture<CustomWebApplicationFactory<ServerApp.Startup>>
    {
        private readonly HttpClient _client;
        private readonly CustomWebApplicationFactory<ServerApp.Startup> _factory;

        public UnitTest1(CustomWebApplicationFactory<ServerApp.Startup> factory)
        {
            _factory = factory;
            _client = factory.CreateClient(new WebApplicationFactoryClientOptions 
            {
                AllowAutoRedirect = false
            });
        }

        [Fact]
        public void Test1()
        {
            Assert.True(true);
        }
    }
}

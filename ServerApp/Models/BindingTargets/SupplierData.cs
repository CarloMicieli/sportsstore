using System.ComponentModel.DataAnnotations;
using System.Text.Json.Serialization;

namespace ServerApp.Models.BindingTargets
{
    public class SupplierData
    {
        [Required]
        public string Name { get; set; }

        [Required]
        public string City { get; set; }

        [Required]
        [StringLength(2, MinimumLength = 2)]
        public string State { get; set; }

        [JsonIgnore]
        public Supplier Supplier => new Supplier {
            Name = Name,
            City = City,
            State = State
        };
    }
}
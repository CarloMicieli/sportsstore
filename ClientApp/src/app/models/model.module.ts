import { Repository } from './repository';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';

@NgModule({
  imports: [HttpClientModule],
  providers: [Repository]
})
export class ModelModule {}
